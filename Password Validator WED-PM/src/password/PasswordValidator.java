package password;

/*
 * @author Ramses Trejo , 023069
 * 
 * This class validates passwords and it will be developed using TDD
 */

public class PasswordValidator {
	
	private static int MIN_LENGTH = 8;
	private static int MIN_NUM_DIGITS = 2;
	
	
	
	public static boolean hasValidCaseChars( String password ) {
		return password != null && password.matches( "^.*[a-z].*$" ) && password.matches( "^.*[A-Z].*$" );
	}

	
/**
 * Validates length of password, blank spaces are not considered valid characters	
 * @param password
 * @return
 */
	
	
	public static boolean isValidLength( String password ) {
		if ( password != null ) {
			return password.trim( ).length( ) >= MIN_LENGTH;
		}
		return false;
	}
	
	public static boolean hasValidDigitCount( String password ) {
		return password != null && password.matches( "([^0-9]*[0-9]){2}.*" );  
	}
	

}
